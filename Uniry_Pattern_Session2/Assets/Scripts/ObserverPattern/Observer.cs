﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour, IObserver
{

    public Material[] observer1Mat;
    private int nbTimeCpt;

    private void Start()
    {
        nbTimeCpt = 0;
    }

    public void onNotify(GameObject entity, string entityEvent)
    {
        switch (entityEvent)
        {
            case "AddComportement":
                if (entity.tag == "Player")
                {
                    if(nbTimeCpt <= 2)
                    {
                        this.gameObject.GetComponent<MeshRenderer>().material = observer1Mat[nbTimeCpt];
                    }
                    nbTimeCpt++;
                }
                break;
        }
    }
}
