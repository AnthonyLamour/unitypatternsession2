﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ISubject
{
    
    void AddObserver(GameObject newObserver);

    void notify(GameObject entity, string entityEvent);

}
