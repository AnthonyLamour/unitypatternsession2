﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeftRight : MonoBehaviour, IComportement
{

    private float xUpdater;
    private float xMin;
    private float xMax;

    public void Execute(LightComportement light)
    {

        xUpdater = light.gameObject.transform.position.x;
        xMin = xUpdater - 2f;
        xMax = xUpdater + 2f;
        StartCoroutine(Moving(light.gameObject));

    }

    IEnumerator Moving(GameObject light)
    {

        
        bool forward = true;
        Vector3 lightPosition = light.transform.position;
        while (true)
        {
            if (forward)
            {
                xUpdater -= 0.5f;
                if (xUpdater <= xMin)
                {
                    forward = false;
                }
            }
            else
            {
                xUpdater += 0.1f;
                if (xUpdater >= xMax)
                {
                    forward = true;
                }
            }
            lightPosition.x = xUpdater;
            light.transform.position = lightPosition;
            yield return new WaitForSeconds(.1f);
        }

    }

}
