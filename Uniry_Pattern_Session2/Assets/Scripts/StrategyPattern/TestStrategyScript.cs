﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestStrategyScript : MonoBehaviour
{

    public GameObject light1;
    public GameObject light2;
    public GameObject light3;

    // Start is called before the first frame update
    void Start()
    {
        light1.GetComponent<LightComportement>().DoSomething(light1.AddComponent<ChangeCouleur>());
        light2.GetComponent<LightComportement>().DoSomething(light2.AddComponent<Clignote>());
        light3.GetComponent<LightComportement>().DoSomething(light3.AddComponent<MoveLeftRight>());
    }

}
