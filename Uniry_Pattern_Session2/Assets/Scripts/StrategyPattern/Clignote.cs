﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clignote : MonoBehaviour, IComportement
{
    public void Execute(LightComportement light)
    {

        StartCoroutine(ColorClignote(light.gameObject));

    }

    IEnumerator ColorClignote(GameObject light)
    {

        byte alphaUpdater = 255;
        bool fadeAway = true;
        Color32 lightColor = light.GetComponent<Renderer>().material.color;
        while (true)
        {
            if (fadeAway)
            {
                alphaUpdater -= 15;
                if (alphaUpdater <= 0)
                {
                    fadeAway = false;
                }
            }
            else
            {
                alphaUpdater += 15;
                if (alphaUpdater >= 255)
                {
                    fadeAway = true;
                }
            }
            lightColor.a = alphaUpdater;
            light.GetComponent<Renderer>().material.color = lightColor;
            yield return new WaitForSeconds(.1f);
        }

    }

}
