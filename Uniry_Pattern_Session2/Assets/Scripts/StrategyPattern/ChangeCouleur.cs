﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCouleur : MonoBehaviour, IComportement
{
    public void Execute(LightComportement light)
    {

        StartCoroutine(ColorChange(light.gameObject));

    }

    IEnumerator ColorChange(GameObject light)
    {
        
        Color32 lightColor = light.GetComponent<Renderer>().material.color;
        while (lightColor.g>0)
        {
            
            lightColor.g -= 5;
            light.GetComponent<Renderer>().material.color = lightColor;
            yield return new WaitForSeconds(.1f);
        }

    }

}
