﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IComportement
{

    void Execute(LightComportement light);

}
