﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler
{

    private ICommand buttonA_;
    private ICommand buttonZ_;
    private ICommand buttonE_;

    public void SetCommand()
    {
        buttonA_ = new ChangeColorCommand();
        buttonZ_ = new ClignoteCommand();
        buttonE_ = new MoveLeftRightCommand();
    }

    public ICommand handleInput()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            return buttonA_;
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            return buttonZ_;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            return buttonE_;
        }

        return null;
    }

}
