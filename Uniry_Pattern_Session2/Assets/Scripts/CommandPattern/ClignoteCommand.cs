﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClignoteCommand : ICommand
{
    public void Execute(GameObject Actor)
    {
        Actor.GetComponent<LightComportement>().DoSomething(Actor.AddComponent<Clignote>());
    }

}