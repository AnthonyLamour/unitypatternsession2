﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, ISubject
{

    private List<Observer> observers;

    private InputHandler PlayerInpuHandler;

    // Start is called before the first frame update
    void Start()
    {
        PlayerInpuHandler = new InputHandler();
        PlayerInpuHandler.SetCommand();
        InitiateObservers();
    }

    // Update is called once per frame
    void Update()
    {
        ICommand command = PlayerInpuHandler.handleInput();
        if (command!=null)
        {
            command.Execute(this.gameObject);
            notify(this.gameObject, "AddComportement");
        }
    }


    public void AddObserver(GameObject newObserver)
    {
        observers.Add(newObserver.GetComponent<Observer>());
    }

    private void InitiateObservers()
    {
        observers = new List<Observer>() { };
        GameObject[] tmpObservers = GameObject.FindGameObjectsWithTag("Observer");
        for(var i = 0; i < tmpObservers.Length; i++)
        {
            AddObserver(tmpObservers[i]);
        }
    }

    public void notify(GameObject entity, string entityEvent)
    {
        for(var i = 0; i < observers.Count; i++)
        {
            observers[i].onNotify(entity, entityEvent);
        }
    }
}
