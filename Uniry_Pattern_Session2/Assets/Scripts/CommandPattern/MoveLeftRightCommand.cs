﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeftRightCommand : ICommand
{
    public void Execute(GameObject Actor)
    {
        Actor.GetComponent<LightComportement>().DoSomething(Actor.AddComponent<MoveLeftRight>());
    }
}
